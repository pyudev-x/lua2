use mlua::prelude::*;
use raylib_ffi::enums::KeyboardKey;
use raylib_ffi::{colors as colors, Color};
use std::fs;
use rand;

#[allow(unused_imports)]
use three_d;

mod functions {
    pub const MAP:&str = "
    return function(arr, func)
        for key, value in pairs(arr) do
            arr[key] = func(value)
        end
        return arr
    end";
}

#[allow(dead_code)]
struct Window {}

struct Key(String);
impl Key {
    fn to_keyboardkey(&self) -> KeyboardKey {
        let value = &self.0;

        match value.to_lowercase().as_str() {
            "a" => return KeyboardKey::A,
            "b" => return KeyboardKey::B,
            "c" => return KeyboardKey::C,
            "d" => return KeyboardKey::D,
            "e" => return KeyboardKey::E,
            "f" => return KeyboardKey::F,
            "g" => return KeyboardKey::G,
            "h" => return KeyboardKey::H,
            "i" => return KeyboardKey::I,
            "j" => return KeyboardKey::J,
            "k" => return KeyboardKey::K,
            "l" => return KeyboardKey::L,
            "m" => return KeyboardKey::M,
            "n" => return KeyboardKey::N,
            "o" => return KeyboardKey::O,
            "p" => return KeyboardKey::P,
            "q" => return KeyboardKey::Q,
            "r" => return KeyboardKey::R,
            "s" => return KeyboardKey::S,
            "t" => return KeyboardKey::T,
            "u" => return KeyboardKey::U,
            "v" => return KeyboardKey::V,
            "w" => return KeyboardKey::W,
            "x" => return KeyboardKey::X,
            "y" => return KeyboardKey::Y,
            "z" => return KeyboardKey::Z,
            " " => return KeyboardKey::Space,
            "space" => return KeyboardKey::Space,
            "backspace" => return KeyboardKey::Backspace,
            "up" => return KeyboardKey::Up,
            "down" => return KeyboardKey::Down,
            "left" => return KeyboardKey::Left,
            "right" => return KeyboardKey::Right,
            _ => return KeyboardKey::Null
        }
    }
}

struct ColorS(String);

impl ColorS {
    fn to_raylib_color(&self) -> Color {
        let value = &self.0;
        match value.to_lowercase().as_str() {
            "red" => return colors::RED,
            "orange" => return colors::ORANGE,
            "yellow" => return colors::YELLOW,
            "green" => return colors::GREEN,
            "blue" => return colors::BLUE,
            "lime" => return colors::LIME,
            "skyblue" => return colors::SKYBLUE,
            "gold" => return colors::GOLD,
            "purple" => return colors::PURPLE,
            "pink" => return colors::PINK,
            "magenta" => return colors::MAGENTA,
            "maroon" => return colors::MAROON,
            "brown" => return colors::BROWN,
            "raywhite" => return colors::RAYWHITE,
            "white" => return colors::WHITE,
            "black" => return colors::BLACK,
            "beige" => return colors::BEIGE,
            "violet" => return colors::VIOLET,
            "gray" => return colors::GRAY,
            "blank" => return colors::BLANK,
            _ => return Color {r: 0, g: 0, b: 0, a: 255 }
        }
    }
}

pub struct Nisshoku(Lua);

impl Nisshoku {
    pub fn new() -> Nisshoku {
        Nisshoku(Lua::new())        
    }

    pub fn run_string(&self,string:String) {
        self.0.load(string).exec().unwrap();
    }

    pub fn run_file(&self, name:String) {
        let code = fs::read_to_string(name).expect("Could not open file. Does it exist?");
        self.0.load(code).exec().unwrap();
    }

    pub fn lua(&self) -> &Lua {
        return &self.0;
    }
    
    pub fn setup_parser(&mut self) {
        // Filesystem
        {            
            let create_file = self.0.create_function(|_, path:String|{
                fs::File::create(path).unwrap();   
                return Ok(());
            }).unwrap();

            let create_dir = self.0.create_function(|_, path:String|{
                fs::create_dir(path).unwrap();
                return Ok(());
            }).unwrap();

            let read = self.0.create_function(|_, path:String|{
                let contents = fs::read_to_string(path).unwrap();
                return Ok(contents);
            }).unwrap();

            let write: LuaFunction<'_> = self.0.create_function(|_, arg:(String, String)|{
                fs::write(arg.0, arg.1).unwrap();
                return Ok(());
            }).unwrap();

            let delete_file = self.0.create_function(|_, path:String|{
                fs::remove_file(path).unwrap();
                return Ok(());
            }).unwrap();

            let delete_dir = self.0.create_function(|_, path:String|{
                fs::remove_dir(path).unwrap();
                return Ok(());
            }).unwrap();
            
            let file_module = self.0.create_table().unwrap();

            file_module.set("createFile", create_file).unwrap();
            file_module.set("createDir", create_dir).unwrap();
            file_module.set("read", read).unwrap();
            file_module.set("write", write).unwrap();
            file_module.set("deleteFile", delete_file).unwrap();
            file_module.set("deleteDir", delete_dir).unwrap();

            self.0.globals().set("fs", file_module).unwrap();
        }

        // HTTP
        {
            use rouille::*;
            
            #[allow(unreachable_code)]
            let listen = self.0.create_function(|_, args:(String, String)|{
                println!("Server started on port: {}",args.0);
                
                start_server((String::from("0.0.0.0:")+args.0.as_str()).as_str(), move |request| {
                    let res = Response::html(args.1.as_str());            
                    println!("Connection Received! Status Code: {} Method: {}", res.status_code, request.method());
                    return res;
                });
                
                

                return Ok(());
            }).unwrap();

            let get = self.0.create_function(|_, arg:String|{
                let request = reqwest::blocking::get(arg).unwrap();
                return Ok(request.text().unwrap());
            }).unwrap();

            let open_url = self.0.create_function(|_, url:String|{
                raylib::open_url(url.as_str());
                Ok(())
            }).unwrap();

            let http_module = self.0.create_table().unwrap();

            http_module.set("listen", listen).unwrap();
            http_module.set("openUrl", open_url).unwrap();
	        http_module.set("get", get).unwrap();

            self.0.globals().set("http", http_module).unwrap(); 
        }

        // Graphics (Unstable)
        {
            unsafe {
                use raylib_ffi::*;
                use std::ffi::CString;

                let graphics = self.0.create_table().unwrap();

                let create_window = self.0.create_function(|_, args:(String,[i32; 2])|{
                    let cstr = CString::new(args.0).unwrap();
                    InitWindow(args.1[0], args.1[1], cstr.as_ptr());
                    SetTargetFPS(60);
                    return Ok(());
                }).unwrap();
                graphics.set("createWindow", create_window).unwrap();

                let window_should_close = self.0.create_function(|_, _args:()|{
                    return Ok(WindowShouldClose());
                }).unwrap();
                graphics.set("windowShouldClose", window_should_close).unwrap();

                let begin_drawing = self.0.create_function(|_, _args:()|{
                    BeginDrawing();
                    return Ok(());
                }).unwrap();
                graphics.set("beginDrawing", begin_drawing).unwrap();

                let mouse_pos = self.0.create_function(|_, _args:()|{
                    return Ok([GetMouseX(), GetMouseY()]);
                }).unwrap();
                graphics.set("mousePos", mouse_pos).unwrap(); 
                
                let end_drawing = self.0.create_function(|_, _args:()|{

                    EndDrawing();

                    return Ok(());
                }).unwrap();
                graphics.set("endDrawing", end_drawing).unwrap();

                let clear = self.0.create_function(|_, args:[u8;4]|{

                    ClearBackground(Color { r: args[0], g: args[1], b: args[2], a: args[3] });

                    return Ok(());
                }).unwrap();
                graphics.set("clear", clear).unwrap();

                let draw_circle = self.0.create_function(|_, args:([i32;2],f32,[u8;4])|{
                    DrawCircle(args.0[0], args.0[1], args.1, Color { r: args.2[0], g: args.2[1], b: args.2[2], a: args.2[3]});
                    return Ok(());
                }).unwrap();
                graphics.set("drawCircle", draw_circle).unwrap();

                let draw_rectangle = self.0.create_function(|_, args:([i32;2],[i32;2],[u8;4])|{
                    DrawRectangle(args.0[0], args.0[1], args.1[0], args.1[1], Color { r: args.2[0], g: args.2[1], b: args.2[2], a: args.2[3]});
                    
                    return Ok(());
                }).unwrap();
                graphics.set("drawRectangle", draw_rectangle).unwrap();
                
                let draw_text= self.0.create_function(|_, args:(String,[i32;2],i32,[u8;4])|{
                    let cstr = CString::new(args.0).unwrap();
                    DrawText(cstr.as_ptr(), args.1[0], args.1[1], args.2, Color { r: args.3[0], g: args.3[1], b: args.3[2], a: args.3[3] });
                    
                    return Ok(());
                }).unwrap();
                graphics.set("drawText", draw_text).unwrap();  

                let set_target_fps = self.0.create_function(|_, arg:i32|{
                    SetTargetFPS(arg);
                    return Ok(())
                }).unwrap();
                graphics.set("setTargetFPS", set_target_fps).unwrap();

                let is_key_down = self.0.create_function(|_, arg:String|{
                    return Ok(IsKeyDown(Key(arg).to_keyboardkey() as i32));
                }).unwrap();
                graphics.set("isKeyDown", is_key_down).unwrap();

                let is_key_pressed = self.0.create_function(|_, arg:String|{
                    return Ok(IsKeyPressed(Key(arg).to_keyboardkey() as i32));
                }).unwrap();
                graphics.set("isKeyPressed", is_key_pressed).unwrap();

                let is_key_released = self.0.create_function(|_, arg:String|{
                    return Ok(IsKeyReleased(Key(arg).to_keyboardkey() as i32));
                }).unwrap();
                graphics.set("isKeyReleased", is_key_released).unwrap();

                let color = self.0.create_function(|_, arg:String|{
                    let col = ColorS(arg).to_raylib_color();
                    return Ok(vec![col.r, col.g, col.b, col.a]);
                }).unwrap();
                graphics.set("color", color).unwrap();

                let draw_texture = self.0.create_function(|_, args:(String, [f32;2],f32,f32,Vec<u8>)|{
                    let cstr = CString::new(args.0).unwrap();
                    let text = LoadTexture(cstr.as_ptr());
                    DrawTextureEx(text, Vector2 { x: args.1[0], y: args.1[1] }, args.2, args.3, Color { r: args.4[0], g: args.4[1], b: args.4[2], a: args.4[3] });
                    return Ok(());
                }).unwrap();
                graphics.set("drawTexture", draw_texture).unwrap();

                self.0.globals().set("raylib", graphics).unwrap();
                
                
            }
        }

        // Graphics (Stable)
        {

        }


        // Utility Functions
        {
            let utils = self.0.create_table().unwrap();
            let replace_string = self.0.create_function(|_, args:(String, String, String)|{
                let value = args.0.replace(&args.1, &args.2);
                return Ok(value)
            }).unwrap();
            utils.set("replaceInString", replace_string).unwrap();

            let map = self.0.load(functions::MAP).into_function().unwrap();
            utils.set("map", map).unwrap();


            self.0.globals().set("utils", utils).unwrap();


            // Globals

            let random_number = self.0.create_function(|_, _:()|{
                return Ok(rand::random::<f32>());
            }).unwrap();

            let random_bool = self.0.create_function(|_, _:()|{
                return Ok(rand::random::<bool>());
            }).unwrap();

            let run_code = self.0.create_function(|lua, arg:String|{
                lua.load(arg).exec().unwrap();
                return Ok(())
            }).unwrap();

            self.0.globals().set("randomNumber", random_number).unwrap();
            self.0.globals().set("randomBool", random_bool).unwrap();
            self.0.globals().set("runCode", run_code).unwrap();
        }

        // Event Listeners
        {
        //     struct EventListener<'a> {
        //         handlers:Vec<LuaFunction<'a>>   
        //     }
            
        //     impl<'a> EventListener<'a> {
        //         pub fn subcribe(&mut self, handler: LuaFunction<'a>) {
        //             self.handlers.push(handler);
        //         }

        //         pub fn emit(&self, value:LuaMultiValue) {
        //             for handler in &self.handlers {
        //                 handler.call::<LuaMultiValue, ()>(value.clone()).unwrap();
        //             }
        //         }
        //     }

        //     impl<'a> LuaUserData for EventListener<'a> {
        //         fn add_methods<'lua, M: LuaUserDataMethods<'lua, Self>>(methods: &mut M) {
        //             methods.add_method_mut("subcribe", |_, this:&mut EventListener<'a>, handler: LuaFunction| {
        //                 this.subcribe(handler);
        //                 Ok(())
        //             });

        //             methods.add_method("emit", move|_, this, value:LuaMultiValue|{
        //                 this.emit(value);
        //                 Ok(())
        //             });
        //         }
        //     }

        //     self.0.globals().set("wow", EventListener{handlers:vec![]}).unwrap();
        }
    }
}
