use std::env;
use nisshoku::*;

fn main() {
    let args = env::args().collect::<Vec<String>>();    
    
    let file_name = args[1].clone();

    let mut magic = Nisshoku::new();
    
    magic.setup_parser();
    magic.run_file(file_name); 
}
