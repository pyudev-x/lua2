# Nisshoku

<img src="./res/logo.png">
<style>
    img {
        width: 128px;
        height: 128px;
    }
</style>

Lua Dialect with extra built-in features for interacting with the Operating System and more!

The name of this language means Eclipse in Japanese

## Warnings

This project is not yet finished and should not be used for application creation.

The graphics is currently unstable using unsafe Raylib bindings and will be rewritten using the `three-d` library

## Installation

Download the source code and run `cargo install --git https://gitlab.com/pyudev-x/lua2`